playazoModule.config(function ($routeProvider) {
    $routeProvider.when('/VPlayas', {
                controller: 'VPlayasController',
                templateUrl: 'app/p/VPlayas.html'
            }).when('/VCrearPlaya', {
                controller: 'VCrearPlayaController',
                templateUrl: 'app/p/VCrearPlaya.html'
            }).when('/VPlaya/:idPlaya', {
                controller: 'VPlayaController',
                templateUrl: 'app/p/VPlaya.html'
            }).when('/VModifPlaya/:idPlaya', {
                controller: 'VModifPlayaController',
                templateUrl: 'app/p/VModifPlaya.html'
            }).when('/VCambios/:idPlaya', {
                controller: 'VCambiosController',
                templateUrl: 'app/p/VCambios.html'
            });
});

playazoModule.controller('VPlayasController', 
   ['$scope', '$location', '$route', 'flash', 'ngDialog', 'ngTableParams', 'pService',
    function ($scope, $location, $route, flash, ngDialog, ngTableParams, pService) {
      $scope.msg = '';
      pService.VPlayas().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
              var VPlaya0Data = $scope.res.data0;
              if(typeof VPlaya0Data === 'undefined') VPlaya0Data=[];
              $scope.tableParams0 = new ngTableParams({
                  page: 1,            // show first page
                  count: 10           // count per page
              }, {
                  total: VPlaya0Data.length, // length of data
                  getData: function($defer, params) {
                      $defer.resolve(VPlaya0Data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  }
              });            


      });
      $scope.VCrearPlaya1 = function() {
        $location.path('/VCrearPlaya');
      };

      $scope.VPlaya0 = function(idPlaya) {
        $location.path('/VPlaya/'+((typeof idPlaya === 'object')?JSON.stringify(idPlaya):idPlaya));
      };

$scope.__ayuda = function() {
ngDialog.open({ template: 'ayuda_VPlayas.html',
        showClose: true, closeByDocument: true, closeByEscape: true});
}    }]);
playazoModule.controller('VCrearPlayaController', 
   ['$scope', '$location', '$route', 'flash', 'pService',
    function ($scope, $location, $route, flash, pService) {
      $scope.msg = '';
      $scope.fPlaya = {};

      pService.VCrearPlaya().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VPlayas1 = function() {
        $location.path('/VPlayas');
      };

      $scope.fPlayaSubmitted = false;
      $scope.ACrearPlaya0 = function(isValid) {
        $scope.fPlayaSubmitted = true;
        if (isValid) {
          
          pService.ACrearPlaya($scope.fPlaya).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              $location.path(label);
              $route.reload();
          });
        }
      };

    }]);
playazoModule.controller('VPlayaController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'pService',
    function ($scope, $location, $route, flash, $routeParams, pService) {
      $scope.msg = '';
      pService.VPlaya({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.VModifPlaya0 = function(idPlaya) {
        $location.path('/VModifPlaya/'+idPlaya);
      };
      $scope.VPlayas1 = function() {
        $location.path('/VPlayas');
      };
      $scope.VCambios2 = function(idPlaya) {
        $location.path('/VCambios/'+idPlaya);
      };

    }]);
playazoModule.controller('VModifPlayaController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'pService',
    function ($scope, $location, $route, flash, $routeParams, pService) {
      $scope.msg = '';
      $scope.fPlaya = {};

      pService.VModifPlaya({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.fPlayaSubmitted = false;
      $scope.AModifPlaya0 = function(isValid) {
        $scope.fPlayaSubmitted = true;
        if (isValid) {
          
          pService.AModifPlaya($scope.fPlaya).then(function (object) {
              var idPlaya = $routeParams.idPlaya;
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              $location.path(label);
              $route.reload();
          });
        }
      };

    }]);
playazoModule.controller('VCambiosController', 
   ['$scope', '$location', '$route', 'flash', '$routeParams', 'ngTableParams', 'pService',
    function ($scope, $location, $route, flash, $routeParams, ngTableParams, pService) {
      $scope.msg = '';
      pService.VCambios({"idPlaya":$routeParams.idPlaya}).then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
              var ARevertir0Data = $scope.res.data0;
              if(typeof ARevertir0Data === 'undefined') ARevertir0Data=[];
              $scope.tableParams0 = new ngTableParams({
                  page: 1,            // show first page
                  count: 10           // count per page
              }, {
                  total: ARevertir0Data.length, // length of data
                  getData: function($defer, params) {
                      $defer.resolve(ARevertir0Data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  }
              });            


      });
      $scope.VPlaya1 = function(idPlaya) {
        $location.path('/VPlaya/'+idPlaya);
      };

      $scope.ARevertir0 = function(id) {
          var tableFields = [["idCambio","id"],["descripcion","Descripción"]];
          var arg = {};
          arg[tableFields[0][1]] = ((typeof id === 'object')?JSON.stringify(id):id);
          pService.ARevertir(arg).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              $location.path(label);
              $route.reload();
          });
      };

    }]);
