playazoModule.service('pService', ['$q', '$http', function($q, $http) {

    this.VPlayas = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/VPlayas',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VCrearPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/VCrearPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.ACrearPlaya = function(fPlaya) {
        return  $http({
          url: "p/ACrearPlaya",
          data: fPlaya,
          method: 'POST',
        });
    //    var labels = ["/VPlaya", "/VCrearPlaya", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/VPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AModifPlaya = function(fPlaya) {
        return  $http({
          url: "p/AModifPlaya",
          data: fPlaya,
          method: 'POST',
        });
    //    var labels = ["/VPlaya", "/VModifPlaya", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VModifPlaya = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/VModifPlaya',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VCambios = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/VCambios',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.ARevertir = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'p/ARevertir',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VPlaya", "/VCambios", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
}]);