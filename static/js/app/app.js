// Creación del módulo de la aplicación
var playazoModule = angular.module('playazo', ['ngRoute', 'ngAnimate', 'ngTable', 'textAngular', 'ngDialog', 'flash']);
playazoModule.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
                controller: 'VPlayasController',
                templateUrl: 'app/p/VPlayas.html'
            });
});
playazoModule.controller('playazoController_',  ['$scope', '$http', '$location',
function($scope) {
    $scope.title = "PLAYAZO";
}]);
