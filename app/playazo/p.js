module.exports = function (app) {
    var module = {};
    var cliente = require('mongodb').MongoClient;
    var objeto = require('mongodb').ObjectId;
    var uri = 'mongodb://linking:chambrai0$@ds047782.mongolab.com:47782/playazo2';
    var assert = require('assert');



app.post('/p/ACrearPlaya', function(request, response) {
    //POST/PUT parameters
    params = request.body;
    var results = [{'label':'/VPlaya/', 'msg':['Playa {{playa.nombre}} creada']}, {'label':'/VCrearPlaya', 'msg':['Error al tratar de crear playa']}, ];
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('playas');
        collection.insert(params, {w: 1}, function(err, records){
            if (err) {
                res = results[1] ;
            } else {
                results = [{'label':'/VPlaya/'+String(params._id), 'msg':['Playa '+'"'+String(params.nombre)+'"'+' creada']}, {'label':'/VCrearPlaya', 'msg':['Error al tratar de crear playa']}, ];
                res = results[0];
                res['idPlaya'] = params._id;
                db.close();
                $next();
            }
        });
    });
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('historico');
        var f = new Date();
        params.fecha = f.getDate() + "/" + ((f.getMonth())+1) + "/" + f.getFullYear();
        params.playa = params._id;
        collection.insert(params, {w: 1}, function(err, records){
            if (err) {
                res = results[1] ;
            } else {
                results = [{'label':'/VPlaya/'+String(params._id), 'msg':['Playa '+'"'+String(params.nombre)+'"'+' creada']}, {'label':'/VCrearPlaya', 'msg':['Error al tratar de crear playa']}, ];
                res = results[0];
                res['idPlaya'] = params._id;
                db.close();
                $next();
            }
        });
    });    
    //Action code ends here
    })(function(){response.json(res);});
    
});
app.post('/p/AModifPlaya', function(request, response) {
    //POST/PUT parameters
    params = request.body;
    results = [{'label':'/VPlaya', 'msg':['Playa {{playa.nombre}} modificada.']}, {'label':'/VModifPlaya'+String(idPlaya), 'msg':['Error al tratar de modificar playa']}, ];
    var res;
    idCambio = params._id;
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
        cliente.connect(uri, function(err,db) {
            if (err) throw err;
            var collection = db.collection('playas');
            var ident = objeto(idPlaya);
            params._id = ident;
            collection.update({_id:ident}, {$set:params}, function(err, records){
                if (err) {
                    res = results[1] ;
                } else {
                db.close();
                }
            });    
        });
        cliente.connect(uri, function(err,db) {
            if(err) throw err;
            var collection = db.collection('playas');
            var ident = objeto(idPlaya);
            collection.findOne({_id:ident}, function(err, item) {
                db.close();
                cliente.connect(uri, function(err,db) {
                    if (err) throw err;
                    console.log(item);
                    results = [{'label':'/VPlaya/'+String(idPlaya), 'msg':['Playa '+'"'+String(item.nombre)+'"'+' modificada']}];
                    res = results[0];
                    res['idPlaya'] = idPlaya;
                    var collection = db.collection('historico');
                    var f = new Date();
                    item.fecha = f.getDate() + "/" + ((f.getMonth())+1) + "/" + f.getFullYear();
                    item.playa = item._id;
                    item._id = idCambio;
                    collection.insert(item, {w: 1}, function(err, records){
                        if (err) {
                            res = results[1] ;
                        } else {
                            db.close();
                            $next();
                        }
                    });
                });
            });
        });        
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/ARevertir', function(request, response) {
    //GET parameter
    idCambio = request.query['id'];
    results = [{'label':'/VPlaya', 'msg':['Cambio revertido']}, {'label':'/VCambios', 'msg':['Error al tratar de revertir un cambio en  playa']}, ];
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
        cliente.connect(uri, function(err,db) {
            if (err) throw err;
            var collection = db.collection('historico');
            idc = objeto(idCambio);
            collection.findOne({_id:idc}, function(err, item) {
                var idPlaya = item.playa;
                results = [{'label':'/VPlaya/'+String(idPlaya), 'msg':['Cambio revertido']}, {'label':'/VCambios', 'msg':['Error al tratar de revertir un cambio en  playa']}];
                res = results[0] ;                
                delete item._id;
                collection = db.collection('playas');
                collection.findOne({_id:idPlaya}, function(err, playa) {
                    collection.update({_id:idPlaya}, {$set:item}, function(err, records){
                        if (err) {
                            res = results[1] ;
                        }
                    });                 
                    collection = db.collection('historico');
                    var f = new Date();
                    item.fecha = f.getDate() + "/" + ((f.getMonth())+1) + "/" + f.getFullYear();
                    collection.insert(item, {w: 1}, function(err, records){
                        if (err) {
                            res = results[1] ;
                        } else {
                            db.close();
                            $next();
                            results = [{'label':'/VPlaya/'+String(idPlaya), 'msg':['Cambio revertido']}];                            
                            res = results[0] ;                            
                        }
                    });
                });
            });
        });
    //Action code ends here    
    })(function(){response.json(res);});

});
app.get('/p/VCambios', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res['idPlaya'] = idPlaya;
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('historico');
        var ident = objeto(idPlaya);
        collection.find({playa:ident}).toArray(function (err, results) {
            var playas = results;
            console.log(playas);
            res['data0'] = playas;
            db.close();
            $next();
        });
    });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VCrearPlaya', function(request, response) {
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VModifPlaya', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    res['idPlaya'] = idPlaya;
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('playas');
        var ident = objeto(idPlaya);
        collection.findOne({_id:ident}, function(err, item) {
            res['playa'] = item;
            db.close();
            $next();
        });
    });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VPlaya', function(request, response) {
    //GET parameter
    idPlaya = request.query['idPlaya'];
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    res['idPlaya'] = idPlaya;
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('playas');
        var ident = objeto(idPlaya);
        collection.findOne({_id:ident}, function(err, item) {
            res['playa'] = item;
            db.close();
            $next();
        });
    });
    //Action code ends here
    })(function(){response.json(res);});

});
app.get('/p/VPlayas', function(request, response) {
    res = {};
    (function ($next) {
    //Action code goes here, res should be a JSON structure call $next() once finished
    cliente.connect(uri, function(err,db) {
        if(err) throw err;
        var collection = db.collection('playas');
        collection.find().toArray(function (err, results) {
            var playas = results;
            res['data0'] = playas;
            db.close();
            $next();
        });
    });
    //Action code ends here
    })(function(){response.json(res);});

});

//Use case code starts here


//Use case code ends here

    return module;

}

